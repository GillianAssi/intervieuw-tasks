#define LED_PIN 13

int counter = 0;
int sleeps = 15;
int interrupttime = 0;
int lastinterrupttime = 0;
void setup() {
  pinMode(LED_PIN, OUTPUT);
  pinMode(3, INPUT);

  attachInterrupt(digitalPinToInterrupt(3), digitalInterrupt, RISING); //interrupt for waking up

  Serial.begin(9600);
  Serial.println("New Reading Started");
  Serial.println("-------------------");
  //SETUP WATCHDOG TIMER
  WDTCSR = (24);//change enable and WDE - also resets
  WDTCSR = (33);//prescalers only - get rid of the WDE and WDCE bit
  WDTCSR |= (1 << 6); //enable interrupt mode

  //ENABLE SLEEP - this enables the sleep mode
  SMCR |= (1 << 2); //power down mode
  SMCR |= 1;//enable sleep
}

void loop() {
  if (sleeps >= 15)
  {
    Serial.print("count: ");
    Serial.println(counter);
    Serial.println("GOING TO SLEEP");
    sleeps = 0;
  }
  digitalWrite(LED_PIN, HIGH);
  delay(50);
  digitalWrite(LED_PIN, LOW);
  //BOD DISABLE - this must be called right before the __asm__ sleep instruction
  MCUCR |= (3 << 5); //set both BODS and BODSE at the same time
  MCUCR = (MCUCR & ~(1 << 5)) | (1 << 6); //then set the BODS bit and clear the BODSE bit at the same time
  __asm__  __volatile__("sleep");//in line assembler to go to sleep
}

void digitalInterrupt() {
  //avoid bouncing
  if (millis() > lastinterrupttime)
  {
    counter++;
    lastinterrupttime = millis() + 50;
  }

  //needed for the digital input interrupt
}

ISR(WDT_vect) {
  sleeps++;
  //DON'T FORGET THIS!  Needed for the watch dog timer.  This is called after a watch dog timer timeout - this is the interrupt function called after waking up
}// watchdog interrupt
