import Redirect from 'umi/redirect';
export default ({match}) =>
    <>
    <Redirect to={{
      pathname: "hello/" + match.params.id
    }}/>
    </>
