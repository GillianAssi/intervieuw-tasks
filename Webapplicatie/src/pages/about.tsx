import router from 'umi/router';
import Link from "umi/link";
import styles from './about.css';

export default function() {
  return (
    <>
      <div className={styles.normal}>
        <h1>About page</h1>
      </div>
      <div id="header">
        <div id="top_info">
          <p>This page contains info about Gillian</p>
        </div>
        <div id="navbar">
          <Link to="/">
            <button>go to form page</button>
          </Link>
        </div>

      </div>
      <hr />
      <div id="content_area">
        <div id="content">
        <p>Mijn naam is Gillian Assi en ben een 1e master student Industriële Wetenschappen: <br/>Elektronica-ICT informatie- en communicatietechnieken met specialisatie netwerken. </p>

        <p>Ik ben gefacineerd door Virtual reality en hoop er in de toekomst mee te werken.<br/></p>
        <p>Mijn hobby's zijn: digitaal tekenen, gamen en koken.</p>
        </div>
      </div>


    </>
  );
}
