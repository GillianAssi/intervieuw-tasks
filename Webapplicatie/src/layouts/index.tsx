import { withRouter } from "react-router";
import dynamic from 'umi/dynamic';
import { connect } from 'dva';

const Context = dynamic({
  loader: () => import('./Context'),
});

function BasicLayout(props) {
  return (
    <div>
      <h2>Web Applications</h2>
      <h3>this webapplication is made by: {props.message}</h3>
      <Context />
      <hr />
      {
        props.children
      }
    </div>
  );
}

function mapStateToProps(state) {
  return {
    message: state.global.message,
  };
}

export default withRouter(connect(mapStateToProps)(BasicLayout));
