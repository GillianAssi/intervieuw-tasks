import React, { Component } from "react";

class Form extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      url: 'hello/'
    }
  }
  handleUsernameChange = (event) => {
    this.setState({
      username: event.target.value,
      url: 'hello/' + event.target.value
    })

  }

  btnClick = (event) => {
    event.preventDefault()
    window.location.href = this.state.url
  }

  render() {
    return (
      <form>
        <div>
          <label>
            Type a name:
          <input type="text" value={this.state.username} onChange={this.handleUsernameChange} />
          <button onClick={this.btnClick} className="button">
            Submit
          </button>
          </label>
        </div>
      </form>

    )

  }
}
export default Form
