import router from 'umi/router';
import styles from './hello.css';
export default ({match}) =>
    <>
      <div>
      <h1>Hello {match.params.id}</h1>
      </div>
      <button onClick={() => {
        router.goBack();
      }}>go back</button>
    </>
