import React, { Component, Fragment } from "react";
import Link from "umi/link";
import styles from "./index.css";
import Form from "@/components/form"


export default () =>

  <Fragment>
    <div className={styles.normal}>
      <h1>Form page</h1>
    </div>
    <div>
      <div>
        <Form />
      </div>
    </div>

    <hr />
    <Link to="/about">
      <button>go to about page</button>
    </Link>

  </Fragment>
